#ifndef LIST_H
#define LIST_H
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <time.h>
#include "scan_int.h"
struct lista{
	int valore;
	struct lista *next;
};
struct lista *crea_lista(int n);
struct lista *aggiungi_testa(struct lista *in,int n);
void stampa(struct lista *a);
struct lista *crea_manuale(int dim);
struct lista *crea_random(int dim);
void elimina_duplicati(struct lista *a);
void shuffle(struct lista *a, struct lista *b);
#endif