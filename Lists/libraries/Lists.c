/**
Lists Subroutines

@author Gaetano Piscopo
*/
#include "lists.h"
struct lista *crea_lista(int n){
	struct lista *a;
	a=malloc(sizeof (struct lista));
	a->valore=n;
	a->next=NULL;
	return a;
}
struct lista *aggiungi_testa(struct lista *in,int n){
	struct lista *a=crea_lista(n);
	a->next=in;
	return a;
	}
void stampa(struct lista *a){
	printf("%d->",a->valore);
	if (a->next!=NULL){

		stampa (a->next);
	}
	else printf("NULL\n");
	return;
}
struct lista *crea_manuale(int dim){
	int i=0;
	printf("inserire elemento 0: ");
	int tmp=scan_int(INT_MIN,INT_MAX);
	struct lista *a=crea_lista(tmp);
	i++;
	for(i=1;i<dim;i++){
		printf("inserire elemento %d: ",i);
		tmp=scan_int(INT_MIN,INT_MAX);
		a=aggiungi_testa(a,tmp);
	}
	return a;
}
struct lista *crea_random(int dim){
	int i=0;
	srand(time(NULL));
	struct lista *a=crea_lista(rand());
	for(i=1;i<dim;i++){
		printf("inserire elemento %d: ",i);
		a=aggiungi_testa(a,rand());
	}
	return a;
}
void elimina_duplicati(struct lista *a){
	if(a->next!=NULL){
		if(a->next->valore==a->valore){
			a->next=a->next->next;
			elimina_duplicati(a);
		}
		else elimina_duplicati(a->next);
	}
	return;
}
void shuffle(struct lista *a, struct lista *b){
	if (a!=NULL && b!=NULL){
		struct lista *tmp1=a->next;
		struct lista *tmp2=b->next;
		b->next=a;
		if (tmp2!=NULL){
			a->next=tmp2;
			shuffle (tmp1,tmp2);
		}
	}
	return;
}