#ifndef LLIST_H
#define LLIST_H
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <limits.h>
#include "scan_int.h"
typedef struct elem{
	struct elem *prev;
	int valore;
	struct elem *next;
}ElemDoppia;
typedef ElemDoppia *listadoppia;
listadoppia CreaDoppia(int n);
listadoppia AggiungiInTestaDoppia(listadoppia L,int n);
void AggiungiInCodaDoppia(listadoppia in,int n);
listadoppia Head(listadoppia in);
listadoppia Tail(listadoppia in);
listadoppia ElimSingolaDoppia(listadoppia in,listadoppia el);
listadoppia ElimElemDoppia(listadoppia in,int elem);
listadoppia CreaDoppiaGrandeRandom(int dimensione);
listadoppia CreaDoppiaGrandeManuale(int dimensione);
void StampaDoppia(listadoppia in);
listadoppia IncrociaInCodaDoppia(listadoppia L1,listadoppia L2);

listadoppia  CreaDCir(int n);
listadoppia AggiungiInTestaDCir(listadoppia in,int n);
void AggiungiInCodaDCir(listadoppia in,int n);
listadoppia CreaDCirGrandeRandom(int dimensione);
void StampaDCir(listadoppia  L);
listadoppia  ElimElemDCir(listadoppia in,int n);
listadoppia  IncrociaDcir(listadoppia  L1,listadoppia  L2);
#endif
