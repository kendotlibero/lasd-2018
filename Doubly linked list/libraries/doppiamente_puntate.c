/**
Doubly Linked and Doubly Linked Circular Lists Subroutines

@author Lorenzo Quaranta
@author Gaetano Piscopo

*/
#include "doppiamente_puntate.h"
//-----Doubly linked list-----

listadoppia CreaDoppia(int n){
	listadoppia a;
	a=(listadoppia )malloc(sizeof (ElemDoppia));
	a->valore=n;
	a->next=NULL;
	a->prev=NULL;
	return a;
}

listadoppia AggiungiInTestaDoppia(listadoppia L,int n){
	listadoppia tmp=(listadoppia )malloc(sizeof(struct elem));
	tmp->prev=NULL;
	tmp->valore=n;
	tmp->next=L;
	L->prev=tmp;
	return tmp;
}

void AggiungiInCodaDoppia(listadoppia in,int n){
	if(in->next==NULL){
		listadoppia a=(listadoppia )malloc(sizeof(struct elem));
		a->prev=in;
		a->valore=n;
		a->next=NULL;
		in->next=a;
	}
	else AggiungiInCodaDoppia(in->next,n);
	return;
}
///the Head and Tail subroutines make the lists that are passsed to them point to their tails or heads
listadoppia Head(listadoppia in){
    while(in->prev!=NULL)
        {
        in=in->prev;
        }
return in;
}
listadoppia Tail(listadoppia in){
    while(in->next!=NULL)
        {
        in=in->next;
        }
return in;
}

///ElimSingolaDoppia eliminates and deallocates a single element from a doubly linked list
listadoppia ElimSingolaDoppia(listadoppia in,listadoppia el){
   if(el!=NULL)
        {                                                                               //If we're pointing at the list's head,it's PREV pointer is NULL.So the new head will also have a NULL PREV pointer
			if (in==el)                                                                 //If we're pointing at it's tail the NEXT pointer will be NULL, so the new tail will also have a NULL NEXT pointer
				in=in->next;                                                            //If we're in the middle ,the element before the one we're eliminating will point to the one that comes after what we're eliminating and vice versa
            if(el->next!=NULL)                                                          //if both PREV and NEXT are null,el is the list's only element
                {                                                                       //in any case,el is deallocated.
                    el->next->prev=el->prev;
                }
            if(el->prev!=NULL)
                {
                    el->prev->next=el->next;
                }

            free(el);
        }                                                                               //if el is null,we simply eliminate the list
    else free(in);
    return in;
}


//This subroutine eliminates all occurrences of a single element from a doubly linked list
listadoppia ElimElemDoppia(listadoppia in,int elem){
    if(in!=NULL)
        {
            listadoppia tmp=in;   //trovato translates as "found" and it counts all eliminations
            int trovato=0;
            while(tmp->prev!=NULL)
                {                             //the list is cycled through,it is assumed that the cycling starts from the tail
                    if(tmp->valore==elem)
                        {
                            trovato++;                                //ElimSingolaDoppia executes elimination and deallocation
                            in=ElimSingolaDoppia(in,tmp);
                        }
                 tmp=tmp->prev;
                }
            if(tmp->valore==elem)
            {
             trovato++;
            in=ElimSingolaDoppia(in,tmp);
            }
            printf("trovato ed eliminato %d volta/e \n",trovato);
        }
    else printf("lista nulla! \n");
	return in;
}

listadoppia CreaDoppiaGrandeRandom(int dimensione){
	int tmp=dimensione;
	srand(time(NULL));
	listadoppia L=CreaDoppia(rand());
	tmp--;
	while(tmp>0){
		L=AggiungiInTestaDoppia(L,rand());
		tmp--;
	}
	return L;
}

listadoppia CreaDoppiaGrandeManuale(int dimensione){
	int tmp=dimensione;
	int i=0;
	printf("inserire elemento %d: ",tmp);
	i=scan_int(INT_MIN,INT_MAX);
	listadoppia L=CreaDoppia(i);
	tmp--;
	while(tmp>0){
		printf("inserire elemento %d: ",tmp);
		i=scan_int(INT_MIN,INT_MAX);
		L=AggiungiInTestaDoppia(L,i);
		tmp--;
	}
	return L;
}
///StampaDoppia prints the list that is passed to it
void StampaDoppia(listadoppia in){
	listadoppia tmp=in;
	while(tmp->next!=NULL){
		printf("%d->",tmp->valore); ///head to tail
		tmp=tmp->next;
	}
	printf("%d->NULL\n",tmp->valore);
	while(tmp!=NULL){                     ///tail to head
		printf("%d<-",tmp->valore);
		tmp=tmp->prev;
	}
	printf("NULL\n");
	return;
}
///this subroutine mixes two doubly linked lists like this L1->L2->L1_>L2 etc etc
listadoppia IncrociaInCodaDoppia(listadoppia L1,listadoppia L2){
	listadoppia L3,tmp,tmp2,el,temp;
	if(L1==NULL && L2==NULL)
        {
            return NULL;
        }
    else if(L1==NULL)
        {                                       ///obvious cases
            L3=L2;
        }
    else if(L2==NULL)
        {
            L3=L1;
        }
    else
        {
            L1=Tail(L1);
            L2=Tail(L2);
            tmp=L1;
            tmp2=L2;
            L3=CreaDoppia(tmp->valore);
            AggiungiInCodaDoppia(L3,tmp2->valore);
            L3=CreaDoppia(tmp->valore);
            AggiungiInCodaDoppia(L3,tmp2->valore);
            tmp2=tmp2->prev;
            tmp=tmp->prev;                            ///L3 is initialized and is given its first two elements(L1 and L2's tails) if L1 and L2 are both not null.This implementation assumes that L1 nad L2 point to their own tails.
            while(tmp!=NULL || tmp2!=NULL)
                {
                    if(tmp==NULL)
                        {
                            AggiungiInCodaDoppia(L3->next,tmp2->valore);
                            temp=tmp2;
                            tmp2=tmp2->prev;
                            free(temp);
                        }
                    else if(tmp2==NULL)                   ///Every single element in L3 is allocated anew and elements from L1 and/or L2 are freed,each time;
                        {
                            AggiungiInCodaDoppia(L3->next,tmp->valore);
                            temp=tmp;
                            tmp=tmp->prev;
                            free(temp);
                        }
                    else
                        {
                            AggiungiInCodaDoppia(L3,tmp->valore);
                            AggiungiInCodaDoppia(L3,tmp2->valore);
                            temp=tmp2;                                                ///two elements from L3 have to be allocated in every cicle if both L! and L2 still exist
                            tmp2=tmp2->prev;                                          ///The PREV and NEXT pointers must be initialized and pointed to the correct element each time
                            free(temp);
                            temp=tmp;
                            tmp=tmp->prev;
                            free(temp);

                        }
                }

        }
        L1=NULL;
        L2=NULL;                     ///the old lists's heads are deallocated here and not in the beginning of the function
	return L3;
}


//-----Doubly linked Circular list-----

listadoppia CreaDCir(int n){
	listadoppia a;
	a=(ElemDoppia *)malloc(sizeof (ElemDoppia));
	a->valore=n;
	a->next=a;
	a->prev=a;
	return a;
}

listadoppia AggiungiInTestaDCir(listadoppia in,int n){
	listadoppia tmp;
	if(in!=NULL){
		tmp=(ElemDoppia *)malloc(sizeof (struct elem));
		tmp->next=in;
		tmp->valore=n;
		tmp->prev=in->prev;
		in->prev->next=tmp;
		in->prev=tmp;
	}
}

void AggiungiInCodaDCir(listadoppia in,int n){
		in->prev=AggiungiInTestaDCir(in->prev,n);
		return;
}

listadoppia CreaDCirGrandeRandom(int dimensione){
	int tmp=dimensione;
	srand(time(NULL));
	listadoppia L=CreaDCir(rand());
	while(dimensione!=0){
		L=AggiungiInTestaDCir(L,rand());
		dimensione--;
	}
	return L;
}

void StampaDCir(listadoppia L){
	if (L!=NULL){
		listadoppia tmp=L;
		do{
			printf("%d->",tmp->valore);
			tmp=tmp->next;
		}
		while (tmp!=L);
	}
	return;
}
//eleminates every occurrency/copy of a single element in a doubly linked circular list
listadoppia ElimElemDCir(listadoppia in,int n)
{
	if(in!=NULL && in->next!=in)
        {
            listadoppia tmp=in;
            listadoppia tmp2;
            listadoppia primo=in;
            while(primo->valore==n)
            {
                primo=primo->next;
                primo->prev->next=primo->next;     ///Checks whether the first element of the list is the one we want to eliminate
                free(tmp);
                tmp=primo;
            }
            tmp=tmp->next;
            while(tmp!=primo)
                {
                                          ///Checks the rest of the list
                    if(tmp->valore==n)
                        {                                                        ///the two elements before and after the one we want to eliminate point to eachother now,the element's mempry is freed
                            tmp2=tmp->next;                                      ///and we continue the cycle from the next element
                            tmp->next->prev=tmp->prev;
                            tmp->prev->next=tmp->next;
                            free(tmp);
                            tmp=tmp2;
                        }
                    else tmp=tmp->next;
                }
            in=primo;
        }
    else if(in->next==in) free(in);
    else printf("la lista è vuota \n");         /// if there is only one element it deletes the list,otherwise it does nothing
	return in;
}


listadoppia IncrociaDcir(listadoppia L1,listadoppia L2)
{
    listadoppia L3;
    //WIP
    return L3;
}
