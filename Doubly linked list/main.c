//liste doppiamente puntate/circolari
#include "libraries\doppiamente_puntate.h"
int main (){
	//Testing the listadoppia structure

	struct elem *a=CreaDoppiaGrandeManuale(10);
	listadoppia b=CreaDoppiaGrandeRandom(13);
	listadoppia c;
	printf("\n\n Lista 1:\n");
	StampaDoppia(a);
	printf("\n\n Lista 2:\n");
	StampaDoppia(b);
	struct elem *tmp=a;
	while (tmp->next!=NULL)
		tmp=tmp->next;
	a=ElimSingolaDoppia(a,tmp);
	printf("\n\n Lista 1 dopo elim:\n");
	StampaDoppia(a);
    c=IncrociaInCodaDoppia(a,b);
    printf("\n\n Liste incrociate:\n");
    StampaDoppia(c);
    printf("\n\n Liste circolari:\n");
	//Testing the listaDcir structure
	listadoppia L;
	L=CreaDCirGrandeRandom(10);
	printf("\n\n");
    StampaDCir(L);
	AggiungiInTestaDCir(L,17);
	AggiungiInTestaDCir(L,34);
	printf("\n\n");
    StampaDCir(L);
    ElimElemDCir(L,17);
    ElimElemDCir(L,34);
    printf("\n\n");
    StampaDCir(L);
	return 0;
}

