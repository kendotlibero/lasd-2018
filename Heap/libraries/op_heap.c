/**
Heap Subroutines
@author Gaetano Piscopo

*/
#include <stdio.h>
#include <limits.h>
#include <stdlib.h>
#include <time.h>
#include "scan_int.h"
int child_sx(int pos){
	return pos*2+1;
}

int child_dx(int pos){
	pos++;
	return pos*2;
}

int parent (int pos){
	if (pos==1 || pos==2) return 0;
	return pos/2;
}

void heapify(int *arr, int pos,int dim){
	int greater=0;
	int s=child_sx(pos);
	int d=child_dx(pos);
	int control=0;
	if (s<dim && arr[s]>arr[pos]) {
		greater=s;
		control=1;
	}
	else if (s<dim){
		greater=pos;
		control=1;
		}
	if (d<dim && arr[d]>arr[greater]){
		greater=d;
		control=1;
		}
	if (greater!=pos && control==1){
		int t;
		t=arr[greater];
		arr[greater]=arr[pos];
		arr[pos]=t;
		heapify(arr,greater,dim);
	}

	return;
}

void build_heap(int *arr,int dim){
	int i=(dim/2);
	for (i=(dim/2);i>=0;i--){
		heapify(arr,i,dim);
		}
	return;
}

int *heap_insertrand(int *arr,int dim){
	srand(time(NULL));
	int i=0;
	for (!i;i<dim;i++){
			arr[i]=rand();
		}
	build_heap(arr,dim);
	return arr;
	}

int *heap_insertmanual(int *arr, const int dim) {
	int i = 0;
	for (!i; i < dim; i++) {
			printf("insert element %d: ", i);
			arr[i] = scan_int(0, INT_MAX);
    }
	build_heap(arr,dim);
	return arr;
}

int *heap_build_rand(int dim){
	int *arr=malloc(sizeof(int)*dim);
	arr=heap_insertrand(arr,dim);
	return arr;
	}

int *heap_build_manual(int dim){
	int *arr=malloc(sizeof(int)*dim);
		arr=heap_insertmanual(arr,dim);
	return arr;
	}

void heapsort (int *arr,int dim){
	//build_heap(arr,dim);
	int i=dim-1;
	int x=dim;
	for (i==dim;i>=1;i--){
		int t;
		t=arr[0];
		arr[0]=arr[i];
		arr[i]=t;
		x--;
		heapify(arr,0,x);
	}
	return;
}

int search (int *arr,int n,int dim){
	int i=0;
	int a=-1;
	for(i=0;i<dim;i++){
		if (arr[i]==n){
			a=i;
			break;
		}
	}
	return a;
}

void heap_down(int *arr,int n,int dim){
	int k=n;
	int child_de=child_dx(k);
	int child_si=child_sx(k);
	int t;
	while (child_de < dim || child_si < dim){
		if (child_de < dim && child_si < dim){
			if (arr[n]>arr[child_de] && arr[n]>arr[child_si]){
				return;
			}
			else if (arr[child_si]<arr[child_de]){
				t=arr[child_si];
				arr[child_si]=arr[k];
				arr[k]=t;
				k=child_si;
				child_de=child_dx(k);
				child_si=child_sx(k);
			}
			else{
				t=arr[child_de];
				arr[child_de]=arr[k];
				arr[k]=t;
				k=child_de;
				child_de=child_dx(k);
				child_si=child_sx(k);
			}
		}
		else{
			if (arr[k]>arr[child_si]) {
				return;
			}
			else {
				t=arr[k];
				arr[k]=arr[child_si];
				arr[child_si]=k;
				child_si=child_sx(k);
			}
		}
	}
	//continua il ceck finchè non è buono l'heap
	return;
}
void heap_up(int *arr,int n,int dim){
	int k=n;
	while (k>0){
		if (arr[k]<arr[parent(k)]) break;
		else{
			int t=0;
			t=arr[k];
			arr[k]=arr[parent(k)];
			arr[parent(k)]=t;
		}
	}
	return;
}

int heap_delete(int *arr,int n, int dim){
	int t=arr[n];
	arr[n]=arr[dim--];
	dim--;
	if (n==0 || arr[parent(n)] > arr[n])
		heap_down(arr,n,dim);
	else heap_up(arr,n,dim);
	return t;
	}
	
	void heap_print(int *arr,int dim){
	int i=0;
	for (i=0;i<dim;i++){
		printf("element %d: %d\n",i,arr[i]);
	}
}

