#ifndef HEAP_H
#define HEAP_H

int child_sx(int pos);
int child_dx(int pos);
int parent (int pos);

void heapify(int *arr, int pos,int dim);
void build_heap(int *arr,int dim);

int *heap_insertrand(int *arr,int dim);
int *heap_insertmanual(int *arr, const int dim);

int *heap_build_rand(int dim);
int *heap_build_manual(int dim);

void heapsort (int *arr,int dim);
int search  (int *arr,int n,int dim);

void heap_down(int *arr,int n,int dim);
void heap_up(int *arr,int n,int dim);
int heap_delete(int *arr,int n, int dim);

void heap_print(int *arr,int dim);

#endif