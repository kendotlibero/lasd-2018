#include <stdio.h>
#include <stdlib.h>
#include "libraries/BST.h"
int main()
{
	int trovato=0;
	albero **a;
	a=initalbero(10);
	Inserisci(a,0);
	Inserisci(a,4);
	Inserisci(a,90);
	Inserisci(a,500);
	Inserisci(a,1999);
	Visita_preordine(a);
	printf("\n");
	Visita_ordine(a);
	printf("\n");
	Visita_postordine(a);
	printf("\n");
	if(Controllo_BST(a)!=0)
		{
			printf("\n è un BST");
			trovato=trovato+ Ricerca(a,4);
			trovato=trovato+ Ricerca(a,8);
			trovato=trovato+ Ricerca(a,1999);
			printf("\n trovati %d elementi\n",trovato);
			elimina(a,4);
			Visita_ordine(a);
			printf("\n");
			elimina(a,500);
			Visita_ordine(a);
			printf("\n");
			elimina(a,10);
			Visita_ordine(a);
			printf("\n");
			elimina(a,90);
		};
	printf("\n");
	 Visita_ordine(a);
	return 0;
}
