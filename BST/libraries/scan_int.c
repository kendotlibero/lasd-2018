#include "scan_int.h"
int convert(int min,int max){
	/*****************************************************************************************************\
	* Questa funzione permette di avere un input sicuro che non faccia loopare il programma. 			  *
	* Ho aggiunto anche dei limitatori di min e max perchè possono sempre servire.						  *
	* Riceve in input una stringa e crea una variabile "pointer_controllo" la funzione controlla se ha    *
	* ricevuto lettere o numeri. Appena riceve una lettera esce e manda indietro un risultato negativo,   *
	* che per mia convenzione è INT_MAX. Altrimenti moltiplicherà pointer_controllo per 10 aggiungendo il *
	* nuovo dato. Se il dato è troppo grande ridarà sempre risultato negativo. Uscito dal for controllerà *
	* se il dato è maggiore di min, in caso positivo ristituirà il numero desiderato.					  *
	* FATTA DA GAETANO PISCOPO	                                                                          *
	* @author Gaetano Piscopo										     	                            						  *
	\*****************************************************************************************************/
	char input[10];
	int input_finale=0;
	int pointer_controllo=0;
	scanf("%s",input);  //prende l'input
		for(pointer_controllo=0;input[pointer_controllo]!=0;pointer_controllo++){
			if (input[pointer_controllo]==0) break;
			if(input[pointer_controllo]<48||input[pointer_controllo]>57) {     //tra 48 e 57 si trovano le rappresentazioni dei mumeri da 0 a 9 in ASCII
				printf("input scorretto,riprovare: ");
				input_finale=INT_MAX;     //segnala errore
				break;
				}
			else {
				input_finale=(input_finale*10);
				input_finale=input_finale+(input[pointer_controllo]-48);    //input[pointer_controllo] è un carattere e verrebbe convertito al suo valore numerico ascii,invece del numero che rappresenta
				if (input_finale>max){
					printf("input troppo grande,riprovare: ");
					input_finale=INT_MAX;                  //segnala errore
					break;
					}
				}
		}
	if (input_finale<min) {
				printf("input troppo piccolo,riprovare: ");
				input_finale=INT_MAX;
			}
	return input_finale;
}
int scan_int (int min, int max){
	//chiama converti finchè non riceve un dato corretto
	int input_finale;
	do{
		//printf("Inserire un numero da %d a %d: ",min,max);
		input_finale=convert(min,max);
	}
	while (input_finale==INT_MAX);   //finchè sei in errore resta nel ciclo
	}
