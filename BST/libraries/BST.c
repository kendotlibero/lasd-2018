/*
Binary Search Tree Subroutines

@author Gaetano Piscopo
@author Lorenzo Quaranta
**/
#include "BST.h"

nodo **initalbero(int n){
	albero **out=malloc(sizeof(nodo*));
	if(!out){
		printf("non allocato");
		return NULL;
	}
	*out=malloc(sizeof(nodo));
	(*out)->left=NULL;
	(*out)->valore=n;
	(*out)->right=NULL;
	return out;
}

int is_null(albero **a){
	if(*a) return 0;
	else return 1;
}

albero **sinistro(albero **a){
	if(*a)
		return (&(*a)->left);
	else return NULL;
}

albero **destro(albero **a){
	if(*a)
	return (&(*a)->right);
	else return NULL;
}
void Inserisci (albero **a,int n){
	albero *aux;
	 if(is_null(a)){
		printf("sto allocando:%d \n",n);
		aux=malloc(sizeof(nodo));
		aux->valore=n;
		aux->right=NULL;
		aux->left=NULL;
		*a=aux;
		
	 }
	 else if((*a)->valore>n){
		printf("%d sinitro->",(*a)->valore);		 
		Inserisci(sinistro(a),n);
	 }
	 else if((*a)->valore<n){
		printf("%d destro->",(*a)->valore);
		Inserisci(destro(a),n);
	 }
	return;
}

void Visita_preordine(albero **a){
	if(!is_null(a)){	
	printf("%d->",(*a)->valore);	
	Visita_preordine(&(*a)->left);
	Visita_preordine(&(*a)->right);
	}
	return;
}

void Visita_ordine(albero **a){
	if(!is_null(a)){	
	Visita_ordine(&(*a)->left);
	printf("%d->",(*a)->valore);	
	Visita_ordine(&(*a)->right);
	}
	return;
}

void Visita_postordine(albero **a){
	if(!is_null(a)){	
	Visita_postordine(&(*a)->left);
	Visita_postordine(&(*a)->right);
	printf("%d->",(*a)->valore);
	}
	return;
}
//---Binary search Tree algorithms

int cerca_minimo(albero **a)
{
	printf("cerco minimo \n");
    if ((*a)->left==NULL)
        return (*a)->valore;
    else return cerca_minimo(sinistro(a));
}

int Ricerca(albero **a,int r)
{
	if (is_null(a))
		return 0;
	else{
		if((*a)->valore==r)
			return 1;
		else if((*a)->valore>r)
			return Ricerca(&((*a)->left),r);
		else return Ricerca(&((*a)->right),r);
	}
	return 0;
}

void elimina(albero **a,int elem)             //needs to be finished
{
	int minimo;
    albero *aux;
    if(is_null(a))
        {
            printf("albero/elemento non trovato \n");            //if we are passed an empty BST it was either empty to begin with
            return;                                              //or the last one of a series of calls to the fuction that passed branches/subtrees trying to find the element
        }
    else
        {
            if(elem<(*a)->valore)
                {
					printf("non trovato,vado a sinistra \n");  
                    elimina(sinistro(a),elem);
                }
            if(elem>(*a)->valore)                       //if the element is not found,we select one of two branches,depending on its value(we assume we have been given a proper BST)
                {
					printf("non trovato,vado a destra \n"); 
                    elimina(destro(a),elem);
                }
            if(elem==(*a)->valore)
                {

					printf("trovato,elimino %d \n",(*a)->valore);
                    if(((*a)->left==NULL)   &&  ((*a)->right==NULL))
                        {
							printf("dealloco %d(a)\n",(*a)->valore); 
                            free(*a);
                            *a=NULL;
                        }
                    else
                    {
                        aux=*a;                     //When the element is found,we worry about the tree structure underneath,whether the element is a leaf or not
                        if((*a)->left==NULL) 
						{
							printf("selezono sottoalbero destro \n"); 							//we first find and select the active subtree and then free the current element
                            *a=aux->right;
						}
                        else if ((*a)->right==NULL)
						{
							printf("selezono sottoalbero sinistro \n");
                            *a=aux->left;
						}
                        if(((*a)->left==NULL)  || ((*a)->right==NULL) ){
								printf("dealloco %d (aux)\n",aux->valore); 
                                free(aux);
                                return;
                            }
                        minimo= cerca_minimo(destro(a));           //if both right and left branch exist ,the smaller element among those that are bigger than the root is sought out and replaces the root,being eleminated in its origianl position
                        (*a)->valore=minimo;
						printf("valore di a:%d \n",(*a)->valore);
                        elimina(destro(a),minimo);
                    }
                }
            }
}

int Controllo_BST(albero **a){
	if(!is_null(a)){
		if((*a)->left){
			if ((*a)->valore<(*a)->left->valore)  //checks whether the object a points to is a BST
			return 0;
		else Controllo_BST (sinistro(a));
		if((*a)->right)
			if ((*a)->valore>(*a)->right->valore)
			return 0;
		}
		else Controllo_BST (destro(a));
	}
	return 1;
}