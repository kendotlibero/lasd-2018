#ifndef BST
#define BST
typedef struct nodo
{
	int valore;
	struct nodo *right;
	struct nodo *left;
}nodo;
typedef nodo albero;
#include "scan_int.h"
#include <stdlib.h>
#include <time.h>
albero **initalbero(int n);
int is_null(albero **a);
albero **sinistro(albero **a);
albero **destro(albero **a);
void Inserisci(albero **a,int n);
void Visita_preordine(albero **a);
void Visita_ordine(albero **a);
void Visita_postordine(albero **a);
int cerca_minimo(albero **a);
int Ricerca(albero **a,int r);
void elimina(albero **a,int elem);
int Controllo_BST(albero **a);
#endif
