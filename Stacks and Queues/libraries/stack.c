//Operazioni su Stack
/**
Stacks and Queues Subroutines

@author Lorenzo Quaranta
*/
///Some basic operations on a stack frame that is implemented on a static array.(See stack.h for full implementation)
///The first four are self explanatory
#include "stack.h"
#include <stdio.h>
int empty_stack(int S[])
{
 return S[0]==0;
}
int full_stack(int S[])
{
 return S[0]== MAX;
}
void push(int S[],int valore)
{
    if(!full_stack(S))
    {
        S[0]=S[0]+1;
        S[S[0]]=valore;
        return;
    }
    else
    {
    printf("Stack pieno!Push non eseguita \n");
    return;
    }
}
int pop(int S[])
{
    if(!empty_stack(S))
    {
        S[0]=S[0]-1;
        return S[S[0]+1];
    }
    else return INT_MAX;
}
///Prints out the contents in the stack using the push and pop subroutines implemented before
void stampa_stack(int S[])
{
  int valore;
  while(!empty_stack(S))
        {
        valore=pop(S);
        printf("pop eseguita. Valore ritrovato:%d \n",valore);
        }

  return;
}

//funzione ricorsiva!!
//presa da un'esercitazione,non so se sia giusta.

///Recursive function that takes a specific element out of the stack
void togli_da_Stack(int S[],int el)
{
int valore;
   if(S[0]=1)
   {
        if(S[1]==el)
            {
            pop(S);   //Caso base della ricorsione
            return;
            }
        else
        {

            return;
        }
   }
   else if (S[S[0]]== el)
   {
       //rimuovi se trovato. Pop senza push.
       pop(S);
       togli_da_Stack(S,el);

   }
   else
   {
    valore=pop(S);
    togli_da_Stack(S,el);
    push(S,valore);       //se non trovato rimuovi in modo che possa analizzare il resto dello stack,ma salva e riponi nello stack quando l'analisi � finita.Pop seguita da push
    }
   return;
}

//initializes the Stack
void initstack(int s[])
{
s[0]=0;
return;
}

//Operazioni su Code
//per il momento funziona che,andando oltre il numero MAX di elementi,la coda si resetta e svuota automaticamente
//inizializza una coda vuota

///Queue subroutines
///Initialization
void initqueue(Coda c)
{
 c[0]=0;   //Head
 c[MAX+1]=1; //Tail
}
int emptyqueue(Coda c)
{
    return c[0]==0;
}

int fullqueue(Coda c)
{
    return c[0]==c[MAX+1];  //controllando il meccanismo che si viene a creare tramite l'uso dell'operatore modulo si capisce il perch� di questa scelta
}
void enqueue(Coda c,int val)
{
    printf("stiamo aggiungendo:%d Head :%d Tail:%d\n",val,c[0],c[MAX+1]);
    if(fullqueue(c))
    {
        printf("Coda piena!\n");
        return;
    }
    c[c[MAX+1]]=val;  //alloca l'elemento nella cella puntata da Tail.
    if(emptyqueue(c))
    {
        c[0]=1;
    }
    /*
    Modifica Tail,facendogli puntare il resto(+1!) tra la divisione tra quello che puntava prima e la dimensione dell'intero array,
    che � uguale al l'indice successivo a quello puntato da Tail tranne il caso in cui Tail punta al valore MAX.
    Algoritmo pensato avendo in mente che gli inserimenti si fanno solo in coda.
    */
    c[MAX+1]=(c[MAX+1]%MAX) +1;          ///Exploits the modulo operator so that it will point to c[1] and continue inserting from there,once insertion has reached over c[MAX+1] without actually filling the Queue
    return;
}
int dequeue(Coda c)
{
    int val;
    val=c[c[0]];  //prendo ci� che � in testa
    c[0]=(c[0]%MAX) +1 ;       // Head passa avanti,stessa logica di sopra.L'algoritmo � pensato avendo in mente che le rimozioni avvengono solo dalla testa
    if(c[0]==c[MAX+1])         ///Same logic as Enqueue but with deletion
    {                          ///also if the pointers are the same after a deletion,the Queue is empty and is the reset to the starting situation
        c[0]=0;
        c[MAX+1]=1;  //Se i due puntatori sono uguali, si"resettano " ad una situazione da coda vuota.Perch� dopo una serie di dequeue,se i due puntatoti coincidono ,la coda � stata svuotata.
    }

    return val;
}
//Funzione ricorsiva,il caso base � la coda vuota

///Recursive printing function
void StampaC(Coda c)
{
    int el;
    printf("che succede qui? Head:%d Tail:%d \n",c[0],c[MAX+1]);
   if(!emptyqueue(c))
    {
        printf("Stampa ,");
        el=dequeue(c);
        printf("estratto:%d \n",el);
        StampaC(c);
        enqueue(c,el);
    }
    return;
}
//Reverse � ricorsiva,il caso base � la coda vuota
///Printing reverses a queue,and this function undoes that accidental reverse
void Reverse(Coda c)
{

    int el;
    if(!emptyqueue(c))
    {
        el=dequeue(c);
        printf("estratto:%d Head:%d Tail:%d \n",el,c[0],c[MAX+1]);
        Reverse(c);
        enqueue(c,el);
    }
    return;
}
///Packs printing and reversing in one function
void StampaCoda(Coda C)
{
   StampaC(C);
   printf(" Reverse \n:");
   Reverse(C);
   return;
}
