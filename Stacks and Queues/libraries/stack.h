#ifndef localstack
#define localstack
#define MAX 20
#include <time.h>
#include <limits.h>
typedef int pila[MAX+1];
typedef int Coda[MAX+2];
//Stack frames are implemented like this:the very first index of the array is used as a pointer to the last element that was inserted on top of the stack,effectively implementing the stack's LIFO logic
/*
Stack implentato avendo il primo elemento come pubntatore all'ultimo elemento inserito,
oppure avente valore MAX se la pila � piena
*/
//the Queue's FIFO logic is bit more comples to implement,this time we have two indexes used as pointers,the Head and the tail. Deletion only happens from the head,and Insertion from the tail.
/*
Invece,le code sono implementate in modo che il primo e l'ultimo elemento diano gli indici dove si trovano la testa e la coda
(Tail conterr� sempre un indice superiore di 1 a quello della cella dove si trova l'ultimo elem allocato,e se la coda � piena l'indice contenuto al suo interno il suo)
*/
//Stack
void push(int S[],int valore);
int pop(int S[]);
int empty_stack(int S[]);
int full_stack(int S[]);
void stampa_stack(int S[]);
void initstack(int s[]);
//Code
void initqueue(Coda c);
int fullqueue(Coda c);
int emptyqueue(Coda c);
void enqueue(Coda c,int val);
int dequeue(Coda c);
void StampaC(Coda c);
void Reverse(Coda c);
void StampaCoda(Coda C);
#endif
