#include <stdio.h>
#include <stdlib.h>
#include "libraries/stack.h"
#define zero(x) x=0
//Testa varie funzioni i cui prototipi si trovano nel file header e che sono implementate in operazioni.c
///testing some functions from operazioni.c
int main()
{
 const int errore = INT_MAX;
 int valore,conto;
 pila S;
 Coda C;
 //testa stack
 initstack(S);
 push(S,1);
 valore=pop(S);
 printf("pop eseguita. Valre ritrovato :%d \n",valore);
 zero(conto);
 srand(time(NULL));
 while(conto<MAX+2)
    {
     push(S,rand()%INT_MAX -1);
     conto++;
    }
stampa_stack(S);

//testa la coda
initqueue(C);
zero(conto);
while(conto<MAX+10)
    {
     printf("posizione %d :",conto);
     enqueue(C,rand()%INT_MAX -1);
     conto++;
    }
StampaCoda(C);
return 0;
}
