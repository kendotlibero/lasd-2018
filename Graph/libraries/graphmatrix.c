/*
  Graph Managing Subroutines(Adjacence Matrix implementation)
  @author Lorenzo Quaranta
  @author Gaetano piscopo
 **/
#include "graphmatrix.h"
graphmat *crea_m(int n){
	graphmat *g;
	g=malloc(sizeof (graphmat));
	//initializing the adjacence matrix
	g->mat=calloc(n,(sizeof(int*)));
	int i=0;
	for(i=0;i<n;i++){
		g->mat[i]=calloc(n,(sizeof(int)));
	}
	g->content=calloc(n,sizeof(int)); 
	g->dim=n;
	return g;
}
void stampa_m (graphmat *g){
	int i,j;
	i=j=0;
	for(i=0;i<g->dim;i++){
		printf("|");
		for(j=0;j<g->dim;j++){
			printf("%d|",g->mat[i][j]);
			}
		printf("         |%d|\n",g->content[i]);
	}
	return;
}

int isEmpty_m(graphmat *g){
	int i,j;
	i=j=0;
	for(i=0;i<g->dim;i++){;
		for(j=0;j<g->dim;j++){
			if (g->mat[i][j]) return 0;
			}
	}
	return 1;
}

void addEdge_m(graphmat *g,int inp,int out){
	if( (inp<g->dim && out<g->dim) && (inp>0 && out>0) && g->mat[inp][out]==0)
		g->mat[inp][out]=1;
	return;
}

void modVert_m(graphmat *g,int n,int val){
	if(n<g->dim && n>0)
		{
		g->content[n]=val;
		}
	return;
}

void delEdge_m(graphmat *g,int inp,int out){
	//if( (inp<g->dim && out<g->dim) && (inp>0 && out>0) && g->mat[inp][out]==1)
		g->mat[inp][out]=0;
	return;
}

void addVert_m(graphmat *g){
	int **secure;
	int i=0;
	g->dim++;
	secure=malloc(g->dim*sizeof(int*));
	for(i=0;i<g->dim-1;i++){
		secure[i]=realloc(g->mat[i],g->dim*sizeof(int));
		secure[i][g->dim-1]=0;
	}
	i=0;
	g->mat=secure;
	g->mat[g->dim-1]=calloc(g->dim,sizeof(int));
	g->content=realloc(g->content,g->dim*sizeof(int));
	g->content[g->dim-1]=0;
	return;
}
void fillvert_m(graphmat *g){
	int i=0;
	for(i=0;i<g->dim;i++){
		g->mat[i][i]=1;
	}
	return;
}
void delvert_m(graphmat *g,int n){
	int i=n;
	int j;
	for (i=n;i<g->dim-1;i++){
		j=0;
		for(j=0;j<g->dim-1;j++){
			g->mat[i][j]=g->mat[i+1][j];
		}
	}
	free(g->mat[g->dim-1]);
	g->dim--;
	int **safe=malloc(g->dim*sizeof(int*));
	i=0;
	for(i=0;i<g->dim;i++){
		safe[i]=realloc(g->mat[i],g->dim*sizeof(int));
	}
	g->mat=safe;
	return;
}

void DFS_m(graphmat *g,int i,int *visto){
	visto[i]=1;
	int x=0;
	for(x=0;x<g->dim;x++){
		if(g->mat[i][x] && !(visto[x])){
			printf("%d",g->content[x]);
			visto[x]=1;
		}
	}
	return;
}

void visit_DFS_m(graphmat *g){
	int i=0;
	int *visto=calloc(g->dim,sizeof(int));
		for(i=0;i<g->dim;i++){
			if(!visto[i]){
				printf("%d",g->content[i]);
				DFS_m(g,i,visto);
			}
	}
}

void es1_2_m(graphmat *g,int i,int *visto){
	visto[i]=1;
	int x=0;
	for(x=0;x<g->dim;x++){
		if(g->mat[i][x] && !(visto[x])){
			visto[x]=1;
		}
		else if (g->mat[i][x] && (visto[x])){
			delEdge_m(g,i,x);
		}
	}
	return;
}

void es1_m(graphmat *g){
	int i=0;
	int *visto=calloc(g->dim,sizeof(int));
		for(i=0;i<g->dim;i++){
			if(!visto[i]){
				es1_2_m(g,i,visto);
			}
	}
}