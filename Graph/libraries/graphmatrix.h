#ifndef GRAPHM_H
#define GRAPHM_H
#include<stdio.h>
#include<stdlib.h>
typedef struct graphmat
{
	int dim;
    int **mat;
    int *content;
} graphmat;
graphmat *crea_m(int n);
void stampa_m(graphmat *g);
int isEmpty_m(graphmat *g);
void addEdge_m(graphmat *g,int inp,int out);
void modVert_m(graphmat *g,int n,int val);
void delEdge_m(graphmat *g,int inp,int out);
void addVert_m(graphmat *g);
void fillvert_m(graphmat *g);
void delvert_m(graphmat *g,int n);
void DFS_m(graphmat *g,int i,int *visto);
void visit_DFS_m(graphmat *g);
void es1_m(graphmat *g);
void es1_2_m(graphmat *g,int i,int *visto);
#endif
