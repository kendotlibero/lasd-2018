#ifndef GRAPH_H
#define GRAPH_H
#include<stdio.h>
#include<stdlib.h>
#include "scan_int.h"
#include "stack.h"
typedef struct edge{						//graphs are implemented through adjancence lists
	int key;								//every **adj matrix is a list of lists,the first element in every list is a node in the graph and the rest of it's own list are its adjacents
	struct edge *next;
} edge;
typedef struct graph{
	int n_vert;
	int n_edge;
	edge **adj;
	int *content;
} graph;
graph *crea(int n);
void Stampa (graph *g);
int isEmpty(graph *g);
void dfs1(struct graph *g, int i, int *aux);
void DepthFirstSearch(graph *g);
void DelG(graph *g);
void AddUEdge(graph *g,int first,int second);
void AddEdge(graph *g,int first,int second);
struct edge *insertOrder(edge *e,edge *new,int *n_edge);
void AddOrientedEdge(graph *g,int first,int second);
void modifyEdge(graph *g,int from,int key,int val);
void ElimEdge(graph *g,int first, int second);
void ElimRecursiveEdge(graph *g,int list);
int EdgeExists(graph *g,int from,int to);
void AddOrModifyVert(graph *g,int val,int vert);
void ElimVert(graph *g,int index);
int compare(graph *g1,graph *g2);
void quicksort (graph **arr,int p, int r);
void es_2(graph **arr,int dim);
void Clean(int *aux,int n_vert);
int SubCheck(struct graph *g, int i, int *aux);
//int OrientedCycleCheck(graph *g)
void TopologicalOrder(graph *g, int i, int *aux,Coda Order);
void CycleElim(graph *g,int *aux,int vert,Coda order);
void Isolate(graph *g,int vert);
void ElimUCycle(graph *g);
#endif