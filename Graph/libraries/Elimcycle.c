/*
this subroutine eliminates unoriented graph cycles from a mixed graph
@author Lorenzo Quaranta
*/
#include "graph.h"
void Clean(int *aux,int n_vert)
{
	int i;
	i=0;
	while( i<n_vert)
	{
		i++;
		aux[i]=0;
	}
	return;
}
int SubCheck(struct graph *g, int i, int *aux) {
edge *e;
aux[i] =aux[i]+ 1;
int found;
found=0;
if (aux[i]>1) 
	found=1;
for(e = g->adj[i]; e; e = e->next)  
	if(EdgeExists(g,e->key,i)==0 && found==0)
{		//due to the fact that we are looking for oriented cycles,it skips any further recursion if we find an unoriented graph
		found=SubCheck(g,e->key,aux);
}
return found;
}

int OrientedCycleCheck(graph *g)
{                                                        //Modified DFS.Works by flagging every vertex every time it passes it.if it flags a vertex more than once it has found a cycle;
int found,i, *aux = calloc(g->n_vert,sizeof(int));
found=0;
if(!aux) 
	{
		printf("Errore di Allocazione\n");
	}
else {
	i = 0;
	while( i < g->n_vert && found==0)   
		{
			found=SubCheck(g,i,aux);
			if(found==0)Clean(aux,g->n_vert); 
			i++;
		}
free(aux);
}
return found;
}
void TopologicalOrder(graph *g, int i, int *aux,Coda Order)
{
if(!aux[i]) enqueue(Order,i);
edge *e;
aux[i] = 1;
for(e = g->adj[i]; e; e = e->next)
	{
		if(!aux[e->key ]) 
		TopologicalOrder(g,e->key,aux,Order);
	}
return;	
}

void Isolate(graph *g,int vert)
{
	int key;
	edge *e;
	e=g->adj[vert];
	while(e!=NULL)
	{
			key=e->key;
			e = e->next;
			if(EdgeExists(g,key,vert)) 
				ElimEdge(g,vert,key); 
			
			
	}
	return;
}
void CycleElim(graph *g,int *aux,int vert,Coda order)
{
	int n,i,key;
	n=g->n_vert;
	i=0;
	edge *e;
	while(i<n && i>-1)
	{
			if(EdgeExists(g,i,vert) && !EdgeExists(g,vert,i))
			{											//if there are any orientated edges going in the vertex,isolate it
			Isolate(g,vert);
			return;                                     
			}
			i++;
	}
	e=g->adj[vert];
	while(e!=NULL)
	{
			key=e->key;                      //If e->key is in the queue it points to a vertex which is next in topological order,we orient any unoriented edge towards it.
			e = e->next;					 //Otherwise we have edges from elements which come before in topological order and as such should be oriented towards the current vertex
			if(present(key,order))
				ElimEdge(g,key,vert);
			else if(EdgeExists(g,key,vert)) 
				ElimEdge(g,vert,key); 
			
	}
}

void ElimUCycle(graph *g){
if(g!=NULL)
	{
	if(OrientedCycleCheck(g))
	{
		printf("esistono cicli orientati,non posso proseguire\n");
		return;
	};
	Coda Order;
	initqueue(Order);
	int i, *aux = calloc(g->n_vert,sizeof(int)); 
	if(!aux) {printf("Errore di Allocazione\n");}
	else {
					for(i = 0; i < g->n_vert; i++)
					if(!aux[i]) TopologicalOrder(g,i,aux,Order);
					for(i = 0; i < g->n_vert; i++)
					CycleElim(g,aux,dequeue(Order),Order);
		}   //Visit and check for unoriented cycles in every vertex until they have all been visited
	free(aux);
	}
return;
}


