/*
  Graph Managing Subroutines(Adjacence Lists implementation)
  @author Lorenzo Quaranta
  @author Gaetano piscopo
 **/
#include "graph.h"
graph *crea(int n){
	if (n==0)
		return NULL;
	graph *g=malloc (sizeof(graph));
	int i=0;
	if (g==NULL) printf("Impossibile allocare memoria");
	else{
		g->adj=malloc(n*sizeof(edge*));
		g->content=calloc(n,sizeof(int));
		if(g->adj==NULL && n>0){
			printf("Impossibile allocare memoria la lista del grafo");
			free(g);
			g=NULL;
		}
		else{
			g->n_vert=n;                                                   //creates an edgeless graph with a precise number of empty vertices
			for(i=0;i<n;i++){
			g->adj[i]=NULL;
			g->n_edge=0;
			}
		}
		return g;
	}
	return g;
}

void Stampa (graph *g){
if(g!=NULL){
	int i=0;
	int n_elem=0;
	edge *e;
	if(!isEmpty(g)){
		//printf("\nIl grafo ha %d vertici  ",g->n_vert);
		for(i=0;i<g->n_vert;i++){
			e=g->adj[i];			//select every vertex's adjacence list
			printf("\n%d:%d->",i,g->content[i]);
			if(e!=NULL)
				{
				while(e!=NULL)
					{
						if(e->key!=-1)									//-1 is used as an error value for edges ,if found,the edge must be eliminated
						{
						printf(" %d(%d) ,",e->key,g->content[e->key]);
						}
						else ElimEdge(g,i,e->key);
						e=e->next;
					}
				}
				else printf(" nessuno  ");
			}
		printf("\b ");
		if (g)
			printf("\n\nIl grafo ha %d archi \n",g->n_edge);
		else printf("il grafo è vuoto");
		return;
		}
	}
	else printf("il grafo è vuoto\n");
return;
}

int isEmpty(graph *g){
	if(g) return 0;
	else return 1;
}

void dfs1(struct graph *g, int i, int *aux) {
edge *e;
aux[i] = 1;
for(e = g->adj[i]; e; e = e->next)
if(!aux[e->key ]) {
	printf("%d(%d),",e->key,g->content[e->key]); dfs1(g,e->key,aux);}
	//printf("\b ");
	return;
}

void DepthFirstSearch(graph *g){	
	if (g){
		int i, *aux = calloc(g->n_vert,sizeof(int));
		if(!aux) 
			printf("Errore di Allocazione\n");
		else {
			for(i = 0; i < g->n_vert; i++)
				if(!aux[i]) {
					printf("\n%d(%d)->( ",i,g->content[i]); 
					dfs1(g,i,aux);printf(")");
				}
			free(aux);
		}
	}
	return;
}

void DelG(graph *g){
	int n;
	n=g->n_vert;
	while(n>0){
		ElimVert(g,n);                  //eliminating a vertex also eliminates any edges associated with it and its adjacency list
		n--;
	}
	free(g);
	return;
}

//Edges

void AddEdge(graph *g,int first,int second){
	if(g){
	AddOrientedEdge(g,first,second);
	AddOrientedEdge(g,second,first);
	return;
	}
	else printf("grafo non esistente");
	return;
}

struct edge *insertOrder(edge *e,edge *new,int *n_edge){
	edge *tmp=NULL;
	if(e){
		if(!e->next){
			if(e->key==new->key){
				*n_edge--;
				return e;
			}
			if (e->key>new->key){
				new->next=e;
				return new;
			}
			else{
				if(e->next)
					tmp=e->next;
				e->next=new;
				if(tmp){
					new->next=tmp;
				}
				return e;
			}
		}
		
		e->next=insertOrder(e->next,new,n_edge);
	}
	return e;
}

void AddOrientedEdge(graph *g,int first,int second){
	if (g){
		edge *new;
		int n=0;
		new=(edge*)malloc(sizeof(edge));
		if (!new){
			printf("allocazione non riuscita");
			return;
		}
		new->key=second;
		new->next=NULL;
		if((first>-1 && second>-1) && (first<g->n_vert &&second<g->n_vert)){
			g->n_edge++;
			if (g->adj[first]==NULL)           //if there is no adjacence list for the vertex indexed by first,and as such it has no edges,the next edge becomes the list's head
					g->adj[first]=new;
			else{                 //otherwise we add it in order
				if(g->adj[first]->key==new->key){
					g->n_edge--;
					return;
					}
				g->adj[first]=insertOrder(g->adj[first],new,&n);
				if(n)
					g->n_edge--;
				}
		}
	}
	else printf("grafo vuoto\n");
	return;
}

void modifyEdge(graph *g,int from,int key,int val){
	//g[from], se ha valore key viene sostituito da val 
	if(g!=NULL){	
			edge *e;
			if((from>-1 && key>-1 &&val>-2) && (from<g->n_vert && key<g->n_vert)){
				if (g->adj[from]==NULL)           //if there is no adjacence list for the vertex indexed by from,return
					return;
				else{
					e=g->adj[from];
					while(e!=NULL){
						if(e->key==key){
							e->key=val;
							return;
						}
						e=e->next;
						}
					}
				}
	}
	return;	
}

void ElimEdge(graph *g,int first, int second){
if(g!=NULL){
	edge *lista,*prev,*next;
	int found=0;
	if((first>=0 && second>=0) && (first<g->n_vert &&second<g->n_vert)){
		lista=g->adj[first];      
		if(lista==NULL)
			return;
		if(lista->key==second){
			g->adj[first]=lista->next;
			//printf("\n elimino arco da %d a %d \n",first,second);
			g->n_edge--;
			free(lista);                 
			return;
			}
	while(lista!=NULL){
		if(lista->next!=NULL && lista->next->key==second){
			prev=lista;
			next=lista->next->next;
			lista=lista->next;	//elimination from a list
			prev->next=next;
			//printf("\n elimino arco da %d a %d \n",first,lista->key);
			g->n_edge--;
			free(lista);
			return;
			}
		lista=lista->next;
		}
		}	
	
	}
	return;
}

//modifyEdge could theoretically create recursive edges,so if any elimination of those is needed you can use this subroutine
void ElimRecursiveEdge(graph *g,int list){
	modifyEdge(g,list,list,-1);
	ElimEdge(g,list,-1);
	return;
}

int EdgeExists(graph *g,int from,int to){
if(g!=NULL)
	{
	edge *e;
	e=g->adj[from];
	while(e!=NULL && e->key<=to)
		{
		if(e->key == to) return 1;	
		e=e->next;
		}
	}
return 0;
}

//Vertices
void AddOrModifyVert(graph *g,int val,int vert){
	//insert vertex in position vert and has value val
	if(g!=NULL){
		int n;
		if(vert<0){                               //if the vert is smaller than 0 or bigger than the g->n_vert parameter we allocate a new vertex,else we modify an existing one
			g->content=realloc(g->content,(g->n_vert+1)*sizeof(int));
			g->content[g->n_vert]=val;
			g->n_vert++;
			n=g->n_vert -1;
			//add new edge list
			g->adj=realloc(g->adj,(g->n_vert)*sizeof(edge*));
			g->adj[g->n_vert-1]=NULL;
		}
		else{
			if(vert<g->n_vert)
				g->content[vert]=val;
			else
				AddOrModifyVert(g,val,-1);
			}
	}
	return;
}
void ElimVert(graph *g,int index){ 
if(g!=NULL){
	int i,j;
	edge *e;
	if(index<g->n_vert)
		{     
			i=index;
			while(i<g->n_vert)
				{
					g->content[i]=g->content[i+1];
					i++;
				}
			//also eliminates every edge in every other list that contains a reference to this vert
			i=0;
			while(i<g->n_vert)
				{
					ElimEdge(g,index,i);
					ElimEdge(g,i,index);
					i++;
				}
				//we also have to correct the edges to have them point at the correct vertex after eliminating one 
			i=0;
			while(i<g->n_vert)
				{
					j=index+1;
					while(j<g->n_vert)
						{
						modifyEdge(g,i,j,j-1);
						j++;
						}
					i++;
				}
			//edge list elimination
			i=index;
			while(i<g->n_vert){
					g->adj[i]=g->adj[i+1];
					i++;
				}
			g->n_vert--;
			}
	else return;
		}
	}

int compare(graph *g1,graph *g2){
	if(!g1 && !g2)
		return 1;
	if (!g1 || !g2)
		return 0;
	int i=0;
	edge *tmp1;
	edge *tmp2;
	if (g1->n_vert!=g2->n_vert)
			return 0;
	for(i=0;i<g1->n_vert;i++){
		tmp1=g1->adj[i];
		tmp2=g2->adj[i];		
		while(tmp1 || tmp2){
			if (tmp1->key!=tmp2->key || !tmp1 || !tmp2)
				return 0;
			tmp1=tmp1->next;
			tmp2=tmp2->next;
			}
	}
	return 1;
}

void swap(graph **a, graph **b){
	graph *t = *a;
	*a = *b;
	*b = t;
}

int partition (graph **arr, int low, int high){
    int pivot=arr[high]->n_edge;
    int i=(low - 1);
    for (int j=low;j<=high-1;j++){
        if (arr[j]->n_edge<=pivot){
			i++;
			swap(&arr[i],&arr[j]);
		}
	}
	swap(&arr[i + 1], &arr[high]);
	return (i + 1);
}

void quicksort(graph **arr,int p,int r){
   if (p < r){
        int pi = partition(arr,p,r);
        quicksort(arr,p,pi-1);
        quicksort(arr,pi+1,r);
    }
}


void es_2(graph **arr,int dim){
	int i=0;
	int j=0;
	if(arr==NULL || dim==1)
		printf("ricerca di coppie non possibile\n");
	else{
		quicksort(arr,0,dim-1);
		for(i=0;i<dim;i++){
				j=i+1;
			for(j=i+1;j<dim && arr[j]->n_edge==arr[i]->n_edge;j++){
				if(compare(arr[i],arr[j]))
					printf("coppia uguale: %d %d\n",i,j);
			}
		}
	}
	return;
}