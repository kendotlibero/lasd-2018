//stampa valori contenuti nel grafo
#include "libraries/graph.h"
int main(){
	printf("Selezionare dimensione dell'array: ");
	int dim=scan_int(0,INT_MAX);
	graph **arr;
	arr=malloc(dim * sizeof(graph*));
	if(!arr)
		printf("allocazione non riuscita");
	int dim_graph;
	int i=0;
	int g;
	int sel;
	int a,b,c;
	graph *tmp;
	printf("selezionare dimensione grafo : ");
	dim_graph=scan_int(0,INT_MAX);
	for(int i=0;i<dim;i++){
		
		tmp=crea(dim_graph);
		g=0;
		while(!g){
			Stampa(tmp);
			printf("selezionare opzione\n1. DFS\n2. aggiungi arco non orientato\n3. aggiungi arco orientato\n4. modifica arco \n5. elimina arco\n6. aggiungi/modifica vertice\n7. elimina vertice\n8.elimina archi non orientati che creno un ciclo\n9. deposita nell'array\n");
			sel=scan_int(1,9);
			switch(sel){
				case 1:
					DepthFirstSearch(tmp);
					break;
				case 2:
					if (tmp){
						printf("nodo 1: ");
						a=scan_int(0,dim_graph);
						printf("nodo 2: ");
						b=scan_int(0,dim_graph);
						AddEdge(tmp,a,b);
					}
					else printf("impossibile aggiungere archi, grafo vuoto\n");
					break;
				case 3:
					if(tmp){
						printf("nodo 1: ");
						a=scan_int(0,dim_graph);
						printf("nodo 2: ");
						b=scan_int(0,dim_graph);
						AddOrientedEdge(tmp,a,b);
					}
					else printf("impossibile aggiungere archi, grafo vuoto\n");
					break;
				case 4:
					if (tmp){
						//g[from], se ha valore key viene sostituito da val 
						printf("selezionare nodo\n");
						a=scan_int(0,dim_graph);
						printf("selezionare chiave da modificare\n");
						b=scan_int(INT_MIN,INT_MAX);
						printf("selezionare con cosa modificare la chiave\n");
						c=scan_int(0,dim_graph);
						modifyEdge(tmp,a,b,c);
					}
					else printf("grafo vuoto, quindi impossibile modificare grafi inesistenti\n");
					break;
				case 5:
					if(tmp){
						printf("selezionare nodo\n");
						a=scan_int(0,dim_graph);
						printf("selezionare la chiave dell'arco da eliminare\n");
						b=scan_int(INT_MIN,INT_MAX);
						ElimEdge(tmp,a,b);
					}
					else printf("impossibile eliminare archi inesistenti\n");
					break;
				case 6:
					if(tmp){
						printf("inserire posizione\n");
						b=scan_int(0,dim_graph);
						printf("dare valore a vertice\n");
						a=scan_int(INT_MIN,INT_MAX);
						AddOrModifyVert(tmp,a,b);
						break;
					}
					else 
						tmp=crea(1);
				case 7:
					printf("selezionare vertice\n");
					a=scan_int(0,dim_graph);
					ElimVert(tmp,a);
					break;
				case 8:
					ElimUCycle(tmp);
					break;
				case 9:
					arr[i]=tmp;
					g++;
					break;
			}
		}
	}
	es_2(arr,dim);
	return 0;

}
